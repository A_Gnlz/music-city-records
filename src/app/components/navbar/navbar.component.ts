import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
    selector: 'navbar-component',
    templateUrl: './navbar.template.html'
})

export class NavbarComponent implements OnInit{
    public title: string;

    constructor(){
        this.title = "Music City Records";
    }

    ngOnInit(){
        $(".button-collapse").sideNav({
            closeOnClick: true
        });
    }
}