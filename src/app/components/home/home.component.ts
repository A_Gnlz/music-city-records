import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
    selector: 'home-component',
    templateUrl: './home.template.html'
})

export class HomeComponent implements OnInit{
    public title: string;
    constructor(){
        this.title = "Music City Records";
    }

    ngOnInit(){
        $('.parallaxHomeBanner').parallax();
        $('.parallaxHomeCards').parallax();
    }
}