import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
    selector: 'gallery-component',
    templateUrl: './gallery.template.html'
})

export class GalleryComponent implements OnInit{
    public title: string;

    constructor(){
        this.title = "Our Gallery";
    }

    ngOnInit(){
        $('.parallaxGalleryBanner').parallax();
        $('.materialboxed').materialbox();
    }
}