import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
    selector: 'contact-component',
    templateUrl: './contact.template.html'
})

export class ContactComponent implements OnInit{
    public title: string;

    constructor(){
        this.title = "Contact Us";
    }

    ngOnInit(){
        $('.parallaxContactBanner').parallax();
    }

}