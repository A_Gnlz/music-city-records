import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
    selector: 'about-component',
    templateUrl: './about.template.html'
})

export class AboutComponent implements OnInit{
    public title: string;

    constructor(){
        this.title = "About Us";
    }

    ngOnInit(){
        $('.parallaxAboutBanner').parallax();
    }
}